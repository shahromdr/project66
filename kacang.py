import requests
import hashlib
import base58
from bit import Key
from aiogram import Bot
import asyncio

API_BASE_URL = 'API_URL'

system_name = "TUKAR_NAMA_SYSTEM"
bot_token = "TUKAR_CHATBOT_API"
chat_id = "TUKAR_TELEGRAM_CHAT_ID"



target_address = '13zb1hQbWVsc2S7ZTZnP2G4undNNpdh5so'

def hash160(b):
    h1 = hashlib.sha256(b).digest()
    r160 = hashlib.new('ripemd160')
    r160.update(h1)
    return r160.digest()

def pubkey_to_address(pubkey, testnet=False):
    prefix = b'\x6f' if testnet else b'\x00'
    return base58.b58encode_check(prefix + hash160(pubkey))

async def send_telegram_message(message):
    bot = Bot(token=bot_token)
    await bot.send_message(chat_id=chat_id, text=message)
    

def get_last_processed_key():
    response = requests.get(f'{API_BASE_URL}/get_last_processed_key')
    if response.status_code != 200 or response.text.strip() == '':
        return None
    data = response.json()
    return int(data['key_hex'], 16)

def update_last_processed_key(key_hex):
    data = {'key_hex': key_hex}
    response = requests.post(f'{API_BASE_URL}/update_last_processed_key', json=data)
    if response.status_code != 200:
        print(response.content)  
        raise Exception("Unable to update last processed key")


counter = 0

def process_key(private_key_number):
    global counter
    counter += 1
    key_hex = format(private_key_number, 'x')

    key = Key.from_int(private_key_number)

    print(f"Number: {private_key_number}")
    print(f"privatekey(HEX): {key.to_hex()}")
    print(f"Private Key (WIF): {key.to_wif()}")
    print(f"Bitcoin Address: {key.address}")
    print(f"Segwit Bitcoin Address: {key.segwit_address}")

    if counter == 1000000:
        telegram_message = discord_message = f"{system_name}\n{private_key_number}"
        asyncio.run(send_telegram_message(telegram_message))
        update_last_processed_key(key_hex)
        counter = 0

    if key.address == target_address: 
        print("Match found!")
        with open("winner.txt", "a") as file:
            file.write('\n==================Matched Address==========================\n')
            file.write("Number: " + str(private_key_number) + "\n")
            file.write("privatekey(HEX): " + key_hex + "\n")
            file.write("Private Key (WIF): " + key.to_wif() + "\n")
            file.write("Bitcoin Address: " + key.address + "\n")
            file.write("Segwit Bitcoin Address: " + key.segwit_address + "\n")
            file.close()

        telegram_message = f"{system_name}\nMatched Address:\nAddress: {private_key_number}\nHEX : {key_hex}\nPrivate Key (WIF): {key.to_wif()}\nBitcoin Address: {key.address}\nSegwit Bitcoin Address: {key.segwit_address}\nhttps://tenor.com/view/bitcoin-gif-11349536"
        print("Telegram Message: ", telegram_message)
        asyncio.run(send_telegram_message(telegram_message))
        

start_of_range = 36893488147419103232
end_of_range = 73786976294838206463

last_progress = get_last_processed_key()

if last_progress:
    start_of_range = last_progress + 1

for private_key_number in range(start_of_range, end_of_range + 1):
    process_key(private_key_number)
